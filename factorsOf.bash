#!/usr/bin/envbash
read -p "Enter a number:" NUM
echo "Factors of ${NUM} are:"

for count in $(seq 1 ${NUM})
do
	if [ $((${NUM}%${count})) == 0 ];then
		echo ${count}
	fi

done
